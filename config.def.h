//Modify this file to change what commands output to your statusbar, and recompile using the make command.
#define SEP " | "
static const Block blocks[] = {
	/*Icon*/   /*Command*/          /*Update Interval*/ /*Update Signal*/
	{"",       "slsrecord",                    0,       2},
	{" b:",    "slsbattery",                   5,       2},
	{SEP,      "date '+%Y-%m-%d %H:%M'",       1,       2},
	{";",      "",                             0,       0},
	{"root:",  "slsdu /",                      60,      1},
	{" home:", "slsdu /home",                  60,      1},
	{SEP,      "slsfree",                      6,       4},
	{";",      "",                             0,       0},
	{" v:",    "slsip virbr0",                 600,     3},
	{" e:",    "slsip enp46s0",                600,     3},
	{" w:",    "slsip wlp45s0",                600,     3},
	{SEP,      "jdate '+%Y/%m/%d'",            3600,    2},
	{SEP,      "xkblayout-state print '%s'",   1,       2},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "";
static unsigned int delimLen = 5;
